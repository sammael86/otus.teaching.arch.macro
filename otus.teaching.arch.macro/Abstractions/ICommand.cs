namespace otus.teaching.arch.macro.Abstractions
{
    public interface ICommand
    {
        void Execute();
    }
}