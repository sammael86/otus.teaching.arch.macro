using otus.teaching.arch.macro.Models;

namespace otus.teaching.arch.macro.Abstractions
{
    public interface IMovable
    {
        Vector Position { get; set; }
        Vector Velocity { get; }
    }
}