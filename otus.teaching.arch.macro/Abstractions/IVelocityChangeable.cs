using otus.teaching.arch.macro.Models;

namespace otus.teaching.arch.macro.Abstractions
{
    public interface IVelocityChangeable : IMovable, IRotatable
    {
        new Vector Velocity { get; set; }
        Vector BaseVelocity { get; set; }
        int BaseAngle { get; set; }
    }
}