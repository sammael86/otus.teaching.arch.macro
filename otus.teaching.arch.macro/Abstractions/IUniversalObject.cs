namespace otus.teaching.arch.macro.Abstractions
{
    public interface IUniversalObject
    {
        object this[string key] { get; set; }
    }
}