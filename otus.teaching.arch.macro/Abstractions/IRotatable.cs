namespace otus.teaching.arch.macro.Abstractions
{
    public interface IRotatable
    {
        int Angle { get; set; }
        int AngularVelocity { get; }
        int MaximumAngles { get; }
    }
}