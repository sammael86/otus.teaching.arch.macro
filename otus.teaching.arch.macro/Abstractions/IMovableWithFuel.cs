namespace otus.teaching.arch.macro.Abstractions
{
    public interface IMovableWithFuel : IMovable, IUseFuel
    {
    }
}