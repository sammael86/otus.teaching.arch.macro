namespace otus.teaching.arch.macro.Abstractions
{
    public interface IUseFuel
    {
        int FuelAmount { get; set; }
        int FuelCost { get; }
    }
}