﻿using System;

namespace otus.teaching.arch.macro.Models
{
    public class Vector : ICloneable
    {
        private readonly int[] _coordinates;

        public Vector(int[] coordinates)
        {
            _coordinates = coordinates;
        }

        public static Vector operator +(Vector vector1, Vector vector2)
        {
            if (vector1._coordinates.Length != vector2._coordinates.Length)
                throw new InvalidOperationException();

            var newCoordinates = new int[vector1._coordinates.Length];
            for (var i = 0; i < vector1._coordinates.Length; i++)
                newCoordinates[i] = vector1._coordinates[i] + vector2._coordinates[i];

            return new Vector(newCoordinates);
        }

        public override bool Equals(object? obj)
        {
            if (obj is not Vector vector)
                return false;

            return this == vector;
        }

        public object Clone()
        {
            return new Vector((_coordinates.Clone() as int[])!);
        }

        public static bool operator ==(Vector vector1, Vector vector2)
        {
            if (vector1._coordinates.Length != vector2._coordinates.Length)
                return false;

            for (var i = 0; i < vector1._coordinates.Length; i++)
                if (vector1._coordinates[i] != vector2._coordinates[i])
                    return false;

            return true;
        }

        public static bool operator !=(Vector vector1, Vector vector2)
        {
            return !(vector1 == vector2);
        }
    }
}