using System;

namespace otus.teaching.arch.macro.Exceptions
{
    public class CommandException : Exception
    {
        public CommandException(string message)
            : base(message)
        {
        }
    }
}