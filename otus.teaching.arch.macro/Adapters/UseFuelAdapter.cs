using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Exceptions;

namespace otus.teaching.arch.macro.Adapters
{
    public class UseFuelAdapter : IUseFuel
    {
        private readonly IUniversalObject _universalObject;

        public UseFuelAdapter(IUniversalObject universalObject)
        {
            _universalObject = universalObject;
        }

        public int FuelAmount
        {
            get
            {
                if (_universalObject[nameof(FuelAmount)] is int fuelAmount)
                    return fuelAmount;

                throw new CommandException($"Get {nameof(FuelAmount)} error");
            }
            set
            {
                if (_universalObject[nameof(FuelAmount)] is not int)
                    throw new CommandException($"Set {nameof(FuelAmount)} error");

                _universalObject[nameof(FuelAmount)] = value;
            }
        }

        public int FuelCost
        {
            get
            {
                if (_universalObject[nameof(FuelCost)] is int fuelCost)
                    return fuelCost;

                throw new CommandException($"Get {nameof(FuelCost)} error");
            }
        }
    }
}