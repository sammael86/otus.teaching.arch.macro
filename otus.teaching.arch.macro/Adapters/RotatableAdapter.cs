using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Exceptions;

namespace otus.teaching.arch.macro.Adapters
{
    public class RotatableAdapter : IRotatable
    {
        private readonly IUniversalObject _universalObject;

        public RotatableAdapter(IUniversalObject universalObject)
        {
            _universalObject = universalObject;
        }

        public int Angle
        {
            get
            {
                if (_universalObject[nameof(Angle)] is int angle)
                    return angle;

                throw new GetAngleException();
            }
            set
            {
                if (_universalObject[nameof(Angle)] is not int)
                    throw new SetAngleException();

                _universalObject[nameof(Angle)] = value;
            }
        }

        public int AngularVelocity
        {
            get
            {
                if (_universalObject[nameof(AngularVelocity)] is int angularVelocity)
                    return angularVelocity;

                throw new GetAngularVelocityException();
            }
        }

        public int MaximumAngles
        {
            get
            {
                if (_universalObject[nameof(MaximumAngles)] is int maximumAngles)
                    return maximumAngles;

                throw new GetMaximumAnglesException();
            }
        }
    }
}