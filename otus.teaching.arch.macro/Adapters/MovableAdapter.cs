using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Exceptions;
using otus.teaching.arch.macro.Models;

namespace otus.teaching.arch.macro.Adapters
{
    public class MovableAdapter : IMovable
    {
        private readonly IUniversalObject _universalObject;

        public MovableAdapter(IUniversalObject universalObject)
        {
            _universalObject = universalObject;
        }

        public Vector Position
        {
            get
            {
                if (_universalObject[nameof(Position)] is Vector position)
                    return position;

                throw new GetPositionException();
            }
            set
            {
                if (_universalObject[nameof(Position)] is not Vector)
                    throw new SetPositionException();

                _universalObject[nameof(Position)] = value;
            }
        }

        public Vector Velocity
        {
            get
            {
                if (_universalObject[nameof(Velocity)] is Vector velocity)
                    return velocity;

                throw new GetVelocityException();
            }
        }
    }
}