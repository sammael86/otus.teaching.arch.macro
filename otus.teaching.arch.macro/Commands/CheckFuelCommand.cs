using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Exceptions;

namespace otus.teaching.arch.macro.Commands
{
    public class CheckFuelCommand : ICommand
    {
        private readonly IUseFuel _useFuel;

        public CheckFuelCommand(IUseFuel useFuel)
        {
            _useFuel = useFuel;
        }

        public void Execute()
        {
            if (_useFuel.FuelAmount < _useFuel.FuelCost)
                throw new CommandException("Not enough fuel.");
        }
    }
}