using System;
using otus.teaching.arch.macro.Abstractions;

namespace otus.teaching.arch.macro.Commands
{
    public class RotateCommand : ICommand
    {
        private readonly IRotatable _rotatable;

        public RotateCommand(IRotatable rotatable)
        {
            _rotatable = rotatable;
        }

        public void Execute()
        {
            _rotatable.Angle =
                (_rotatable.Angle + Math.Abs(_rotatable.AngularVelocity) * _rotatable.MaximumAngles +
                    _rotatable.AngularVelocity) %
                _rotatable.MaximumAngles;
        }
    }
}