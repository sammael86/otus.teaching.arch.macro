using otus.teaching.arch.macro.Abstractions;

namespace otus.teaching.arch.macro.Commands
{
    public class BurnFuelCommand : ICommand
    {
        private readonly IUseFuel _useFuel;

        public BurnFuelCommand(IUseFuel useFuel)
        {
            _useFuel = useFuel;
        }

        public void Execute()
        {
            _useFuel.FuelAmount -= _useFuel.FuelCost;
        }
    }
}