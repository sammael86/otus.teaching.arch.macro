using System.Collections.Generic;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Exceptions;

namespace otus.teaching.arch.macro.Commands
{
    public class MacroCommand : ICommand
    {
        private readonly IEnumerable<ICommand> _commands;

        public MacroCommand(IEnumerable<ICommand> commands)
        {
            _commands = commands;
        }

        public void Execute()
        {
            foreach (var command in _commands)
            {
                try
                {
                    command.Execute();
                }
                catch (CommandException e)
                {
                    throw new CommandException($"Error during macro command execution: {e.Message}");
                }
            }
        }
    }
}