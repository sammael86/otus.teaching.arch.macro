using otus.teaching.arch.macro.Abstractions;

namespace otus.teaching.arch.macro.Commands
{
    public class MoveWithFuelCommand : ICommand
    {
        private readonly MacroCommand _macroCommand;

        public MoveWithFuelCommand(IMovableWithFuel movableWithFuel)
        {
            _macroCommand = new MacroCommand(new ICommand[]
            {
                new CheckFuelCommand(movableWithFuel),
                new MoveCommand(movableWithFuel),
                new BurnFuelCommand(movableWithFuel)
            });
        }

        public void Execute()
        {
            _macroCommand.Execute();
        }
    }
}