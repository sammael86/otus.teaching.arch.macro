using System;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Models;

namespace otus.teaching.arch.macro.Commands
{
    public class ChangeVelocityCommand : ICommand
    {
        private readonly IVelocityChangeable _velocityChangeable;

        public ChangeVelocityCommand(IVelocityChangeable velocityChangeable)
        {
            _velocityChangeable = velocityChangeable;
        }

        public void Execute()
        {
            if (_velocityChangeable.BaseVelocity is not { })
            {
                _velocityChangeable.BaseVelocity = (_velocityChangeable.Velocity.Clone() as Vector)!;
                _velocityChangeable.BaseAngle = _velocityChangeable.Angle;
            }

            var angle = _velocityChangeable.BaseAngle;
            var newAngle = (_velocityChangeable.Angle +
                    Math.Abs(_velocityChangeable.AngularVelocity) * _velocityChangeable.MaximumAngles +
                    _velocityChangeable.AngularVelocity) %
                _velocityChangeable.MaximumAngles;

            Vector newVelocity = (_velocityChangeable.BaseVelocity.Clone() as Vector)!;

            while (angle != newAngle)
            {
                // newVelocity = ...

                angle = (angle + Math.Abs(_velocityChangeable.AngularVelocity) * _velocityChangeable.MaximumAngles +
                    _velocityChangeable.AngularVelocity) % _velocityChangeable.MaximumAngles;
            }

            _velocityChangeable.Velocity = newVelocity;
        }
    }
}