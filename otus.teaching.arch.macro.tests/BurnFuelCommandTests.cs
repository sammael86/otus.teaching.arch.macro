using Moq;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Adapters;
using otus.teaching.arch.macro.Commands;
using otus.teaching.arch.macro.Exceptions;
using Xunit;

namespace otus.teaching.arch.macro.tests
{
    public class BurnFuelCommandTests
    {
        private readonly Mock<IUseFuel> _useFuelMock;
        private readonly Mock<IUniversalObject> _universalObjectMock;

        public BurnFuelCommandTests()
        {
            _useFuelMock = new Mock<IUseFuel>();
            _universalObjectMock = new Mock<IUniversalObject>();

            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelAmount)])
                .Returns(10);
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelCost)])
                .Returns(1);
        }

        [Theory]
        [InlineData(10, 4, 6)]
        [InlineData(4, 4, 0)]
        public void BurnFuel_FuelBurned_NewFuelAmountSet(int fuelAmount, int fuelCost, int newFuelAmount)
        {
            // Arrange
            _useFuelMock
                .SetupGet(uf => uf.FuelAmount)
                .Returns(fuelAmount)
                .Verifiable();
            _useFuelMock
                .SetupGet(uf => uf.FuelCost)
                .Returns(fuelCost)
                .Verifiable();
            _useFuelMock
                .SetupSet(uf => uf.FuelAmount = It.Is<int>(fa => fa == newFuelAmount));

            var command = new BurnFuelCommand(_useFuelMock.Object);

            // Act
            command.Execute();

            // Assert
            _useFuelMock.VerifyAll();
        }

        [Fact]
        public void BurnFuel_FuelAmountCannotBeSet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupSet(uo => uo[nameof(IUseFuel.FuelAmount)] = It.IsAny<int>())
                .Throws(new CommandException($"Set {nameof(IUseFuel.FuelAmount)} error"))
                .Verifiable();
        
            var command = new BurnFuelCommand(new UseFuelAdapter(_universalObjectMock.Object));
        
            // Act
        
            // Assert
            var exceptionMessage = Assert.Throws<CommandException>(() => command.Execute()).Message;
            Assert.Equal($"Set {nameof(IUseFuel.FuelAmount)} error", exceptionMessage);
            _universalObjectMock.Verify();
        }
    }
}