using System.Collections.Generic;
using Moq;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Commands;
using otus.teaching.arch.macro.Models;
using Xunit;

namespace otus.teaching.arch.macro.tests
{
    public class MoveWithFuelCommandTests
    {
        private readonly Mock<IMovableWithFuel> _movableWithFuelMock;

        public MoveWithFuelCommandTests()
        {
            _movableWithFuelMock = new Mock<IMovableWithFuel>();
        }

        public static IEnumerable<object[]> MoveWithFuelExecute_CommandSuccess_TestData()
        {
            yield return new object[]
            {
                new Vector(new[] { 12, 5 }),
                new Vector(new[] { 3, 9 }),
                10,
                2
            };
            yield return new object[]
            {
                new Vector(new[] { 0, 0 }),
                new Vector(new[] { -1, 2 }),
                4,
                4
            };
        }

        [Theory]
        [MemberData(nameof(MoveWithFuelExecute_CommandSuccess_TestData))]
        public void MoveWithFuelExecute_CommandSuccess_PositionChangedAndFuelBurned(Vector position, Vector velocity,
            int fuelAmount, int fuelCost)
        {
            // Arrange
            _movableWithFuelMock
                .SetupGet(m => m.Position)
                .Returns(position)
                .Verifiable();
            _movableWithFuelMock
                .SetupGet(m => m.Velocity)
                .Returns(velocity)
                .Verifiable();
            _movableWithFuelMock
                .SetupGet(m => m.FuelAmount)
                .Returns(fuelAmount)
                .Verifiable();
            _movableWithFuelMock
                .SetupGet(m => m.FuelCost)
                .Returns(fuelCost)
                .Verifiable();

            _movableWithFuelMock
                .SetupSet(m => m.Position = It.Is<Vector>(p => p == position + velocity))
                .Verifiable();
            _movableWithFuelMock
                .SetupSet(m => m.FuelAmount = It.Is<int>(fa => fa == fuelAmount - fuelCost));

            var command = new MoveWithFuelCommand(_movableWithFuelMock.Object);

            // Act
            command.Execute();

            // Assert
            _movableWithFuelMock.VerifyAll();
        }
    }
}