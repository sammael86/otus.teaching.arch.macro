using Moq;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Adapters;
using otus.teaching.arch.macro.Commands;
using otus.teaching.arch.macro.Exceptions;
using Xunit;

namespace otus.teaching.arch.macro.tests
{
    public class CheckFuelCommandTests
    {
        private readonly Mock<IUseFuel> _useFuelMock;
        private readonly Mock<IUniversalObject> _universalObjectMock;

        public CheckFuelCommandTests()
        {
            _useFuelMock = new Mock<IUseFuel>();
            _universalObjectMock = new Mock<IUniversalObject>();

            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelAmount)])
                .Returns(10);
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelCost)])
                .Returns(1);
        }

        [Theory]
        [InlineData(10, 4)]
        [InlineData(4, 4)]
        public void CheckFuel_EnoughFuel_NoExceptions(int fuelAmount, int fuelCost)
        {
            // Arrange
            _useFuelMock
                .SetupGet(uf => uf.FuelAmount)
                .Returns(fuelAmount)
                .Verifiable();
            _useFuelMock
                .SetupGet(uf => uf.FuelCost)
                .Returns(fuelCost)
                .Verifiable();

            var command = new CheckFuelCommand(_useFuelMock.Object);

            // Act
            command.Execute();

            // Assert
            _useFuelMock.VerifyAll();
        }

        [Theory]
        [InlineData(0, 4)]
        [InlineData(3, 4)]
        public void CheckFuel_NotEnoughFuel_ThrowsException(int fuelAmount, int fuelCost)
        {
            // Arrange
            _useFuelMock
                .SetupGet(uf => uf.FuelAmount)
                .Returns(fuelAmount)
                .Verifiable();
            _useFuelMock
                .SetupGet(uf => uf.FuelCost)
                .Returns(fuelCost)
                .Verifiable();

            var command = new CheckFuelCommand(_useFuelMock.Object);

            // Act

            // Assert
            var exceptionMessage = Assert.Throws<CommandException>(() => command.Execute()).Message;
            Assert.Equal("Not enough fuel.", exceptionMessage);
            _useFuelMock.VerifyAll();
        }

        [Fact]
        public void CheckFuel_FuelAmountCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelAmount)])
                .Returns(null)
                .Verifiable();

            var command = new CheckFuelCommand(new UseFuelAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            var exceptionMessage = Assert.Throws<CommandException>(() => command.Execute()).Message;
            Assert.Equal($"Get {nameof(IUseFuel.FuelAmount)} error", exceptionMessage);
            _universalObjectMock.Verify();
        }

        [Fact]
        public void CheckFuel_FuelCostCannotBeGet_ThrowsException()
        {
            // Arrange
            _universalObjectMock
                .SetupGet(uo => uo[nameof(IUseFuel.FuelCost)])
                .Returns(null)
                .Verifiable();

            var command = new CheckFuelCommand(new UseFuelAdapter(_universalObjectMock.Object));

            // Act

            // Assert
            var exceptionMessage = Assert.Throws<CommandException>(() => command.Execute()).Message;
            Assert.Equal($"Get {nameof(IUseFuel.FuelCost)} error", exceptionMessage);
            _universalObjectMock.Verify();
        }
    }
}