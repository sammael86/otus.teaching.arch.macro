using System.Collections.Generic;
using Moq;
using otus.teaching.arch.macro.Abstractions;
using otus.teaching.arch.macro.Commands;
using otus.teaching.arch.macro.Exceptions;
using Xunit;

namespace otus.teaching.arch.macro.tests
{
    public class MacroCommandTests
    {
        private readonly Mock<ICommand> _commandMock;
        private readonly Mock<ICommand> _badCommandMock;

        public MacroCommandTests()
        {
            _commandMock = new Mock<ICommand>();
            _badCommandMock = new Mock<ICommand>();

            _badCommandMock
                .Setup(c => c.Execute())
                .Throws(new CommandException("Test command exception"));
        }

        [Theory]
        [InlineData(3)]
        [InlineData(5)]
        public void MacroCommandExecute_AllCommandsIsGood_AllCommandsExecuted(int commandsAmount)
        {
            // Arrange
            var commands = new List<ICommand>();
            for (var i = 0; i < commandsAmount; i++)
                commands.Add(_commandMock.Object);

            var macroCommand = new MacroCommand(commands);

            // Act
            macroCommand.Execute();

            // Assert
            _commandMock.Verify(c => c.Execute(), Times.Exactly(commandsAmount));
        }

        [Theory]
        [InlineData(3, 1)]
        [InlineData(5, 4)]
        public void MacroCommandExecute_CommandsContainsBadCommand_ThrowsException(int commandsAmount,
            int indexOfBadCommand)
        {
            // Arrange
            var commands = new List<ICommand>();
            for (var i = 0; i < commandsAmount; i++)
                commands.Add(_commandMock.Object);
            commands[indexOfBadCommand] = _badCommandMock.Object;

            var macroCommand = new MacroCommand(commands);

            // Act

            // Assert
            var exceptionMessage = Assert.Throws<CommandException>(() => macroCommand.Execute()).Message;
            Assert.Equal("Error during macro command execution: Test command exception", exceptionMessage);
            _commandMock.Verify(c => c.Execute(), Times.Exactly(indexOfBadCommand));
        }
    }
}